@echo off
@echo on
cls
:start
@echo ------------------------------------------------
@echo "Clear the distributeddata store dir and config.json"
@echo %date%-%time%
@echo "Input the cygwin install dir like D:\cygwin64"
@set /p cypwin_dir=Please input(full name):
rd /s /q %cypwin_dir%\data\test
rd /s /q %cypwin_dir%\data\service\el1\public\database\distributeddata
mkdir %cypwin_dir%\data\service\el1\public\database\distributeddata\meta
mkdir %cypwin_dir%\data\service\el1\public\database\distributeddata\meta\backup
mkdir %cypwin_dir%\data\service\el1\public\database\distributeddata\kvdb
mkdir %cypwin_dir%\data\test
@echo "SUCCESS"
pause