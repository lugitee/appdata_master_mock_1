@echo off
@echo on
cls
:start
@echo ------------------------------------------------

@echo %date%-%time%
mkdir tmp
@echo "clone the datamgr_service"
git clone https://gitee.com/openharmony/distributeddatamgr_datamgr_service.git ./tmp/datamgr_service
@echo "clone the kv_store"
git clone https://gitee.com/openharmony/distributeddatamgr_kv_store.git ./tmp/kv_store
@echo "clone the relational_store"
git clone https://gitee.com/openharmony/distributeddatamgr_relational_store.git ./tmp/relational_store
@echo "clone the data_object"
git clone https://gitee.com/openharmony/distributeddatamgr_data_object.git ./tmp/data_object
@echo "clone the data_share"
git clone https://gitee.com/openharmony/distributeddatamgr_data_share.git ./tmp/data_share
@echo "clone the preferences"
git clone https://gitee.com/openharmony/distributeddatamgr_preferences.git ./tmp/preferences
xcopy tmp\* ..\..\ /y /e /i /q
rd /s /q tmp
@echo "FINISHED"
pause