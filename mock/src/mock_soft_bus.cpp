/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <map>
#include <securec.h>
#include <session.h>
#include <string>

#include "softbus_bus_center.h"
static std::map<std::string, std::map<std::string, const ISessionListener *>> sessionListeners;
static std::map<std::string, INodeStateCb *> callbacks_;
static int32_t g_sessionId = 0;
int32_t UnregNodeDeviceStateCb(INodeStateCb *callback)
{
    return 0;
}
int32_t RegNodeDeviceStateCb(const char *pkgName, INodeStateCb *callback)
{
    callbacks_[pkgName] = callback;
    return 0;
}
int32_t GetAllNodeDeviceInfo(const char *pkgName, NodeBasicInfo **info, int32_t *infoNum)
{
    return 0;
}
void FreeNodeInfo(NodeBasicInfo *info) {}
int32_t GetLocalNodeDeviceInfo(const char *pkgName, NodeBasicInfo *info)
{
    if (pkgName == nullptr || info == nullptr) {
        return -1;
    }
    // (void)strcpy_s(info->networkId, NETWORK_ID_BUF_LEN, "1252645812135842132135452A3B4C5D6E7F");
    (void)strcpy_s(info->networkId, NETWORK_ID_BUF_LEN,
        "54a0a92a428005db27c40bad46bf145fede38ec37effe0347cd990fcb031f320");
    (void)strcpy_s(info->deviceName, DEVICE_NAME_BUF_LEN, "demo phone");
    info->deviceTypeId = SMART_PHONE;
    return 0;
}

int32_t GetNodeKeyInfo(const char *pkgName, const char *networkId, NodeDeviceInfoKey key, uint8_t *info,
    int32_t infoLen)
{
    auto nodeId = std::string(networkId);
    if (nodeId == std::string("no_exist_device_id") || nodeId.find("invalid_device") != std::string::npos) {
        return -1;
    }
    (void)strcpy_s((char *)info, infoLen, networkId);
    return 0;
}

int OpenSession(const char *mySessionName, const char *peerSessionName, const char *peerDeviceId, const char *groupId,
    const SessionAttribute *attr)
{
    int32_t id = g_sessionId++;
    for (const auto &[key, sessions] : sessionListeners) {
        if (key != "ohos.distributeddata" && key != "ohos.objectstore") {
            continue;
        }
        auto it = sessions.find(mySessionName);
        if (it != sessions.end() && it->second != nullptr) {
            it->second->OnSessionOpened(id, 0);
            //            it->second->OnSessionClosed(id);
        }
    }
    return id;
}

int SendBytes(int sessionId, const void *data, unsigned int len)
{
    return -1;
}

int CreateSessionServer(const char *pkgName, const char *sessionName, const ISessionListener *listener)
{
    auto it = sessionListeners.find(pkgName);
    if (it == sessionListeners.end()) {
        auto itTmp = sessionListeners.emplace(pkgName, std::map<std::string, const ISessionListener *>());
        if (itTmp.second) {
            it = itTmp.first;
        }
    }
    if (it == sessionListeners.end()) {
        return -1;
    }
    it->second[sessionName] = listener;
    return 0;
}

int RemoveSessionServer(const char *pkgName, const char *sessionName)
{
    return 0;
}
int GetMySessionName(int sessionId, char *sessionName, unsigned int len)
{
    return 0;
}
int GetPeerSessionName(int sessionId, char *sessionName, unsigned int len)
{
    return 0;
}
int GetPeerDeviceId(int sessionId, char *devId, unsigned int len)
{
    return 0;
}
void CloseSession(int sessionId){};
int GetSessionSide(int sessionId)
{
    return 1;
};

int32_t SetNodeDataChangeFlag(const char *pkgName, const char *networkId, uint16_t dataChangeFlag)
{
    auto it = callbacks_.find(pkgName);
    if (it == callbacks_.end()) {
        return -1;
    }
    NodeStatus status;
    status.dataBaseStatus = dataChangeFlag;
    // (void)strcpy_s(info->networkId, NETWORK_ID_BUF_LEN, "1252645812135842132135452A3B4C5D6E7F");
    (void)strcpy_s(status.basicInfo.networkId, NETWORK_ID_BUF_LEN,
        "64a0a92a428005db27c40bad46bf145fede38ec37effe0347cd990fcb031f320");
    (void)strcpy_s(status.basicInfo.deviceName, DEVICE_NAME_BUF_LEN, "demo phone");
    status.basicInfo.deviceTypeId = SMART_PHONE;
    it->second->onNodeStatusChanged(TYPE_DATABASE_STATUS, &status);
    return 0;
}

int GetSessionOption(int sessionId, SessionOption option, void *optionValue, uint32_t valueSize)
{
    auto val = reinterpret_cast<uint32_t *>(optionValue);
    *val = 1024;
    return 0;
}