/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "resource_manager.h"
namespace OHOS::Global::Resource {
ResourceManager::~ResourceManager() {}
class ResManagerImpl : public ResourceManager {
public:
    ~ResManagerImpl() override {}
    bool AddResource(const char *path) override
    {
        return false;
    }
    RState UpdateResConfig(ResConfig &resConfig) override
    {
        return LOCALEINFO_IS_NULL;
    }
    void GetResConfig(ResConfig &resConfig) override {}
    RState GetStringById(uint32_t id, std::string &outValue) override
    {
        return LOCALEINFO_IS_NULL;
    }
    RState GetStringByName(const char *name, std::string &outValue) override
    {
        return LOCALEINFO_IS_NULL;
    }
    RState GetStringFormatById(std::string &outValue, uint32_t id, ...) override
    {
        return LOCALEINFO_IS_NULL;
    }
    RState GetStringFormatByName(std::string &outValue, const char *name, ...) override
    {
        return LOCALEINFO_IS_NULL;
    }
    RState GetStringArrayById(uint32_t id, std::vector<std::string> &outValue) override
    {
        return LOCALEINFO_IS_NULL;
    }
    RState GetStringArrayByName(const char *name, std::vector<std::string> &outValue) override
    {
        return LOCALEINFO_IS_NULL;
    }
    RState GetPatternById(uint32_t id, std::map<std::string, std::string> &outValue) override
    {
        return LOCALEINFO_IS_NULL;
    }
    RState GetPatternByName(const char *name, std::map<std::string, std::string> &outValue) override
    {
        return LOCALEINFO_IS_NULL;
    }
    RState GetPluralStringById(uint32_t id, int quantity, std::string &outValue) override
    {
        return LOCALEINFO_IS_NULL;
    }
    RState GetPluralStringByName(const char *name, int quantity, std::string &outValue) override
    {
        return LOCALEINFO_IS_NULL;
    }
    RState GetPluralStringByIdFormat(std::string &outValue, uint32_t id, int quantity, ...) override
    {
        return LOCALEINFO_IS_NULL;
    }
    RState GetPluralStringByNameFormat(std::string &outValue, const char *name, int quantity, ...) override
    {
        return LOCALEINFO_IS_NULL;
    }
    RState GetThemeById(uint32_t id, std::map<std::string, std::string> &outValue) override
    {
        return LOCALEINFO_IS_NULL;
    }
    RState GetThemeByName(const char *name, std::map<std::string, std::string> &outValue) override
    {
        return LOCALEINFO_IS_NULL;
    }
    RState GetBooleanById(uint32_t id, bool &outValue) override
    {
        return LOCALEINFO_IS_NULL;
    }
    RState GetBooleanByName(const char *name, bool &outValue) override
    {
        return LOCALEINFO_IS_NULL;
    }
    RState GetIntegerById(uint32_t id, int &outValue) override
    {
        return LOCALEINFO_IS_NULL;
    }
    RState GetIntegerByName(const char *name, int &outValue) override
    {
        return LOCALEINFO_IS_NULL;
    }
    RState GetFloatById(uint32_t id, float &outValue) override
    {
        return LOCALEINFO_IS_NULL;
    }
    RState GetFloatById(uint32_t id, float &outValue, std::string &unit) override
    {
        return LOCALEINFO_IS_NULL;
    }
    RState GetFloatByName(const char *name, float &outValue) override
    {
        return LOCALEINFO_IS_NULL;
    }
    RState GetFloatByName(const char *name, float &outValue, std::string &unit) override
    {
        return LOCALEINFO_IS_NULL;
    }
    RState GetIntArrayById(uint32_t id, std::vector<int> &outValue) override
    {
        return LOCALEINFO_IS_NULL;
    }
    RState GetIntArrayByName(const char *name, std::vector<int> &outValue) override
    {
        return LOCALEINFO_IS_NULL;
    }
    RState GetColorById(uint32_t id, uint32_t &outValue) override
    {
        return LOCALEINFO_IS_NULL;
    }
    RState GetColorByName(const char *name, uint32_t &outValue) override
    {
        return LOCALEINFO_IS_NULL;
    }
    RState GetProfileById(uint32_t id, std::string &outValue) override
    {
        return LOCALEINFO_IS_NULL;
    }
    RState GetProfileByName(const char *name, std::string &outValue) override
    {
        return LOCALEINFO_IS_NULL;
    }
    RState GetProfileDataByName(const char *name, size_t len, std::unique_ptr<uint8_t[]> &outValue) override
    {
        return LOCALEINFO_IS_NULL;
    }
    RState GetMediaById(uint32_t id, std::string &outValue) override
    {
        return LOCALEINFO_IS_NULL;
    }
    RState GetMediaById(uint32_t id, uint32_t density, std::string &outValue) override
    {
        return LOCALEINFO_IS_NULL;
    }
    RState GetMediaByName(const char *name, std::string &outValue) override
    {
        return LOCALEINFO_IS_NULL;
    }
    RState GetMediaByName(const char *name, uint32_t density, std::string &outValue) override
    {
        return LOCALEINFO_IS_NULL;
    }
    RState GetRawFilePathByName(const std::string &name, std::string &outValue) override
    {
        return LOCALEINFO_IS_NULL;
    }
    RState GetRawFileDescriptor(const std::string &name, RawFileDescriptor &descriptor) override
    {
        return LOCALEINFO_IS_NULL;
    }
    RState CloseRawFileDescriptor(const std::string &name) override
    {
        return LOCALEINFO_IS_NULL;
    }
};
class ResConfigImpl : public ResConfig {
public:
    RState SetLocaleInfo(const char *language, const char *script, const char *region) override
    {
        return LOCALEINFO_IS_NULL;
    }
    void SetDeviceType(DeviceType deviceType) override {}
    void SetDirection(Direction direction) override {}
    void SetScreenDensity(ScreenDensity screenDensity) override {}
    void SetColorMode(ColorMode colorMode) override {}
    void SetMcc(uint32_t mcc) override {}
    void SetMnc(uint32_t mnc) override {}
    Direction GetDirection() const override
    {
        return DIRECTION_HORIZONTAL;
    }
    ScreenDensity GetScreenDensity() const override
    {
        return SCREEN_DENSITY_XXXLDPI;
    }
    DeviceType GetDeviceType() const override
    {
        return DEVICE_CAR;
    }
    ColorMode GetColorMode() const override
    {
        return DARK;
    }
    uint32_t GetMcc() const override
    {
        return 0;
    }
    uint32_t GetMnc() const override
    {
        return 0;
    }
    bool Copy(ResConfig &other) override
    {
        return false;
    }
};

ResourceManager *CreateResourceManager()
{
    return new ResManagerImpl();
}

ResConfig *CreateResConfig()
{
    return new ResConfigImpl();
}
}