/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "napi_base_context.h"
#include "napi_common_want.h"
#include "napi_common_util.h"
#include "module_manager/native_module_manager.h"
namespace OHOS::AbilityRuntime {
napi_status IsStageContext(napi_env env, napi_value object, bool& stageMode)
{
    stageMode = true;
    return napi_ok;
}
std::shared_ptr<Context> GetStageModeContext(napi_env env, napi_value object)
{
    return nullptr;
}
}
namespace OHOS::AppExecFwk {
napi_value WrapWantParams(napi_env env, const AAFwk::WantParams &wantParams) { return nullptr; };
napi_value WrapWant(napi_env env, const Want &want) { return nullptr; }
bool UnwrapWant(napi_env env, napi_value param, Want &want) { return false; }
int UnwrapInt32FromJS(napi_env env, napi_value param, int defaultValue) { return 0; }
std::string UnwrapStringFromJS(napi_env env, napi_value param, const std::string &defaultValue) { return defaultValue; }
bool UnwrapArrayStringFromJS(napi_env env, napi_value param, std::vector<std::string> &value) { return true; }
napi_value WrapVoidToJS(napi_env env) { return nullptr; }
bool IsArrayForNapiValue(napi_env env, napi_value param, uint32_t &arraySize) { return true; }
napi_value GetCallbackErrorValue(napi_env env, int errCode) { return nullptr; }
bool UnwrapInt32ByPropertyName(napi_env env, napi_value jsObject, const char *propertyName, int32_t &value) { return true; }
bool UnwrapStringByPropertyName(napi_env env, napi_value jsObject, const char *propertyName, string &value) { return true; }
}
NativeModuleManager *NativeModuleManager::instance_ = new NativeModuleManager;
NativeModuleManager::NativeModuleManager() {}
NativeModuleManager::~NativeModuleManager() {}
NativeModuleManager* NativeModuleManager::GetInstance() { return instance_; }
uint64_t NativeModuleManager::Release() { return 0; }
void NativeModuleManager::Register(NativeModule *nativeModule) { firstNativeModule_ = nativeModule; }
void NativeModuleManager::SetAppLibPath(const char *appLibPath) {}
NativeModule *NativeModuleManager::LoadNativeModule(const char *moduleName, const char *path, bool isAppModule,
                                                    bool internal, bool isArk) { return nullptr; }
void NativeModuleManager::SetNativeEngine(std::string moduleName, NativeEngine *nativeEngine) {}
const char *NativeModuleManager::GetModuleFileName(const char *moduleName, bool isAppModule) { return "Test"; }