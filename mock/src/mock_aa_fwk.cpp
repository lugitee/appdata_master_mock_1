/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "ability_manager_client.h"
#include "launch_param.h"
#include "ability_connect_callback_stub.h"
#include "dataobs_mgr_client.h"
#include "data_ability_observer_stub.h"
namespace OHOS::AAFwk {
bool LaunchParam::ReadFromParcel(Parcel &parcel) { return false; }
bool LaunchParam::Marshalling(Parcel &parcel) const { return false; }
LaunchParam *LaunchParam::Unmarshalling(Parcel &parcel) { return nullptr; }
AbilityManagerClient::AbilityManagerClient() {}
AbilityManagerClient::~AbilityManagerClient() {}
std::shared_ptr<AbilityManagerClient> AbilityManagerClient::GetInstance()
{
    static std::shared_ptr<AbilityManagerClient> instance = std::make_shared<AbilityManagerClient>();
    return instance;
}
ErrCode AbilityManagerClient::AttachAbilityThread(const sptr<IAbilityScheduler> &scheduler,
                                                  const sptr<IRemoteObject> &token) { return 0; }
ErrCode AbilityManagerClient::AbilityTransitionDone(const sptr<IRemoteObject> &token, int state, const PacMap &saveData)
{
    return 0;
}
ErrCode AbilityManagerClient::ScheduleConnectAbilityDone(const sptr<IRemoteObject> &token,
                                                         const sptr<IRemoteObject> &remoteObject) { return 0; }
ErrCode AbilityManagerClient::ScheduleDisconnectAbilityDone(const sptr<IRemoteObject> &token) { return 0; }
ErrCode AbilityManagerClient::ScheduleCommandAbilityDone(const sptr<IRemoteObject> &token) { return 0; }
AppExecFwk::ElementName AbilityManagerClient::GetTopAbility() { return AppExecFwk::ElementName(); }
ErrCode AbilityManagerClient::StartAbility(const Want &want, int requestCode, int32_t userId) { return 0; }
ErrCode AbilityManagerClient::StartAbility(const Want &want, const sptr<IRemoteObject> &callerToken, int requestCode,
                                           int32_t userId) { return 0; }
ErrCode AbilityManagerClient::StartAbility(const Want &want, const AbilityStartSetting &abilityStartSetting,
                                           const sptr<IRemoteObject> &callerToken, int requestCode, int32_t userId)
{
    return 0;
}
ErrCode AbilityManagerClient::StartAbility(const Want &want, const StartOptions &startOptions,
                                           const sptr<IRemoteObject> &callerToken, int requestCode, int32_t userId)
{
    return 0;
}
ErrCode AbilityManagerClient::StartExtensionAbility(const Want &want, const sptr<IRemoteObject> &callerToken,
                                                    int32_t userId, AppExecFwk::ExtensionAbilityType extensionType)
{
    return 0;
}
ErrCode AbilityManagerClient::StopExtensionAbility(const Want &want, const sptr<IRemoteObject> &callerToken,
                                                   int32_t userId, AppExecFwk::ExtensionAbilityType extensionType)
{
    return 0;
}
ErrCode AbilityManagerClient::TerminateAbility(const sptr<IRemoteObject> &token, int resultCode, const Want *resultWant)
{
    return 0;
}
ErrCode AbilityManagerClient::SendResultToAbility(int requestCode, int resultCode, Want &resultWant) { return 0; }
ErrCode AbilityManagerClient::CloseAbility(const sptr<IRemoteObject> &token, int resultCode, const Want *resultWant)
{
    return 0;
}
ErrCode AbilityManagerClient::TerminateAbility(const sptr<IRemoteObject> &callerToken, int requestCode) { return 0; }
ErrCode AbilityManagerClient::TerminateAbilityResult(const sptr<IRemoteObject> &token, int startId) { return 0; }
ErrCode AbilityManagerClient::MinimizeAbility(const sptr<IRemoteObject> &token, bool fromUser) { return 0; }
ErrCode AbilityManagerClient::ConnectAbility(const Want &want, const sptr<IAbilityConnection> &connect, int32_t userId)
{
    return 0;
}
ErrCode AbilityManagerClient::ConnectAbility(const Want &want, const sptr<IAbilityConnection> &connect,
                                             const sptr<IRemoteObject> &callerToken, int32_t userId) { return 0; }
ErrCode AbilityManagerClient::DisconnectAbility(const sptr<IAbilityConnection> &connect) { return 0; }
sptr<IAbilityScheduler> AbilityManagerClient::AcquireDataAbility(const Uri &uri, bool tryBind,
                                                                 const sptr<IRemoteObject> &callerToken)
{
    return sptr<IAbilityScheduler>();
}
ErrCode AbilityManagerClient::ReleaseDataAbility(sptr<IAbilityScheduler> dataAbilityScheduler,
                                                 const sptr<IRemoteObject> &callerToken) { return 0; }
ErrCode AbilityManagerClient::DumpState(const std::string &args, std::vector<std::string> &state) { return 0; }
ErrCode AbilityManagerClient::DumpSysState(const std::string &args, std::vector<std::string> &state, bool isClient,
                                           bool isUserID, int UserID) { return 0; }
ErrCode AbilityManagerClient::Connect() { return 0; }
ErrCode AbilityManagerClient::StopServiceAbility(const Want &want) { return 0; }
ErrCode AbilityManagerClient::KillProcess(const std::string &bundleName) { return 0; }
ErrCode AbilityManagerClient::ClearUpApplicationData(const std::string &bundleName) { return 0; }
sptr<IWantSender> AbilityManagerClient::GetWantSender(const WantSenderInfo &wantSenderInfo,
                                                      const sptr<IRemoteObject> &callerToken) { return sptr<IWantSender>(); }
ErrCode AbilityManagerClient::SendWantSender(const sptr<IWantSender> &target,
                                             const SenderInfo &senderInfo) { return 0; }
void AbilityManagerClient::CancelWantSender(const sptr<IWantSender> &sender) {}
ErrCode AbilityManagerClient::GetPendingWantUid(const sptr<IWantSender> &target, int32_t &uid) { return 0; }
ErrCode AbilityManagerClient::GetPendingWantUserId(const sptr<IWantSender> &target, int32_t &userId) { return 0; }
ErrCode AbilityManagerClient::GetPendingWantBundleName(const sptr<IWantSender> &target,
                                                       std::string &bundleName) { return 0; }
ErrCode AbilityManagerClient::GetPendingWantCode(const sptr<IWantSender> &target, int32_t &code) { return 0; }
ErrCode AbilityManagerClient::GetPendingWantType(const sptr<IWantSender> &target, int32_t &type) { return 0; }
void AbilityManagerClient::RegisterCancelListener(const sptr<IWantSender> &sender,
                                                  const sptr<IWantReceiver> &recevier) {}
void AbilityManagerClient::UnregisterCancelListener(const sptr<IWantSender> &sender,
                                                    const sptr<IWantReceiver> &recevier) {}
ErrCode AbilityManagerClient::GetPendingRequestWant(const sptr<IWantSender> &target, std::shared_ptr<Want> &want)
{
    return 0;
}
ErrCode AbilityManagerClient::GetWantSenderInfo(const sptr<IWantSender> &target, std::shared_ptr<WantSenderInfo> &info)
{
    return 0;
}
void AbilityManagerClient::GetSystemMemoryAttr(AppExecFwk::SystemMemoryAttr &memoryInfo) {}
ErrCode AbilityManagerClient::GetAppMemorySize() { return 0; }
bool AbilityManagerClient::IsRamConstrainedDevice() { return false; }
ErrCode AbilityManagerClient::ContinueMission(const std::string &srcDeviceId, const std::string &dstDeviceId,
                                              int32_t missionId, const sptr<IRemoteObject> &callback,
                                              WantParams &wantParams) { return 0; }
ErrCode AbilityManagerClient::StartContinuation(const Want &want, const sptr<IRemoteObject> &abilityToken,
                                                int32_t status) { return 0; }
void AbilityManagerClient::NotifyCompleteContinuation(const std::string &deviceId, int32_t sessionId, bool isSuccess)
{
}
ErrCode AbilityManagerClient::ContinueAbility(const std::string &deviceId, int32_t missionId, uint32_t versionCode)
{
    return 0;
}
ErrCode AbilityManagerClient::NotifyContinuationResult(int32_t missionId, int32_t result) { return 0; }
ErrCode AbilityManagerClient::LockMissionForCleanup(int32_t missionId) { return 0; }
ErrCode AbilityManagerClient::UnlockMissionForCleanup(int32_t missionId) { return 0; }
ErrCode AbilityManagerClient::RegisterMissionListener(const sptr<IMissionListener> &listener) { return 0; }
ErrCode AbilityManagerClient::UnRegisterMissionListener(const sptr<IMissionListener> &listener) { return 0; }
ErrCode AbilityManagerClient::RegisterMissionListener(const std::string &deviceId,
                                                      const sptr<IRemoteMissionListener> &listener) { return 0; }
ErrCode AbilityManagerClient::UnRegisterMissionListener(const std::string &deviceId,
                                                        const sptr<IRemoteMissionListener> &listener) { return 0; }
ErrCode AbilityManagerClient::GetMissionInfos(const std::string &deviceId, int32_t numMax,
                                              std::vector<MissionInfo> &missionInfos) { return 0; }
ErrCode AbilityManagerClient::GetMissionInfo(const std::string &deviceId, int32_t missionId, MissionInfo &missionInfo)
{
    return 0;
}
ErrCode AbilityManagerClient::GetMissionSnapshot(const std::string &deviceId, int32_t missionId,
                                                 MissionSnapshot &snapshot, bool isLowResolution) { return 0; }
ErrCode AbilityManagerClient::CleanMission(int32_t missionId) { return 0; }
ErrCode AbilityManagerClient::CleanAllMissions() { return 0; }
ErrCode AbilityManagerClient::MoveMissionToFront(int32_t missionId) { return 0; }
ErrCode AbilityManagerClient::MoveMissionToFront(int32_t missionId, const StartOptions &startOptions) { return 0; }
ErrCode AbilityManagerClient::GetMissionIdByToken(const sptr<IRemoteObject> &token, int32_t &missionId) { return 0; }
ErrCode AbilityManagerClient::StartAbilityByCall(const Want &want,
                                                 const sptr<IAbilityConnection> &connect) { return 0; }
ErrCode AbilityManagerClient::StartAbilityByCall(const Want &want, const sptr<IAbilityConnection> &connect,
                                                 const sptr<IRemoteObject> &callToken) { return 0; }
ErrCode AbilityManagerClient::ReleaseAbility(const sptr<IAbilityConnection> &connect,
                                             const AppExecFwk::ElementName &element) { return 0; }
ErrCode AbilityManagerClient::GetAbilityRunningInfos(std::vector<AbilityRunningInfo> &info) { return 0; }
ErrCode AbilityManagerClient::GetExtensionRunningInfos(int upperLimit, std::vector<ExtensionRunningInfo> &info)
{
    return 0;
}
ErrCode AbilityManagerClient::GetProcessRunningInfos(std::vector<AppExecFwk::RunningProcessInfo> &info) { return 0; }
ErrCode AbilityManagerClient::StartSyncRemoteMissions(const std::string &devId, bool fixConflict,
                                                      int64_t tag) { return 0; }
ErrCode AbilityManagerClient::StopSyncRemoteMissions(const std::string &devId) { return 0; }
ErrCode AbilityManagerClient::StartUser(int accountId) { return 0; }
ErrCode AbilityManagerClient::StopUser(int accountId, const sptr<IStopUserCallback> &callback) { return 0; }
ErrCode AbilityManagerClient::RegisterSnapshotHandler(const sptr<ISnapshotHandler> &handler) { return 0; }
ErrCode AbilityManagerClient::StartUserTest(const Want &want, const sptr<IRemoteObject> &observer) { return 0; }
ErrCode AbilityManagerClient::FinishUserTest(const std::string &msg, const int64_t &resultCode,
                                             const std::string &bundleName) { return 0; }
ErrCode AbilityManagerClient::GetTopAbility(sptr<IRemoteObject> &token) { return 0; }
ErrCode AbilityManagerClient::DelegatorDoAbilityForeground(const sptr<IRemoteObject> &token) { return 0; }
ErrCode AbilityManagerClient::DelegatorDoAbilityBackground(const sptr<IRemoteObject> &token) { return 0; }
ErrCode AbilityManagerClient::DoAbilityForeground(const sptr<IRemoteObject> &token, uint32_t flag) { return 0; }
ErrCode AbilityManagerClient::DoAbilityBackground(const sptr<IRemoteObject> &token, uint32_t flag) { return 0; }
int AbilityManagerClient::SetAbilityController(const sptr<AppExecFwk::IAbilityController> &abilityController,
                                               bool imAStabilityTest) { return 0; }
ErrCode AbilityManagerClient::SendANRProcessID(int pid) { return 0; }
ErrCode AbilityManagerClient::FreeInstallAbilityFromRemote(const Want &want, const sptr<IRemoteObject> &callback,
                                                           int32_t userId, int requestCode) { return 0; }
ErrCode AbilityManagerClient::DumpAbilityInfoDone(std::vector<std::string> &infos,
                                                  const sptr<IRemoteObject> &callerToken) { return 0; }
sptr<IAbilityManager> AbilityManagerClient::GetAbilityManager() { return sptr<IAbilityManager>(); }
void AbilityManagerClient::ResetProxy(const wptr<IRemoteObject> &remote) {}
void AbilityManagerClient::HandleDlpApp(Want &want) {}
AbilityConnectionStub::AbilityConnectionStub() {}
AbilityConnectionStub::~AbilityConnectionStub() {}
int AbilityConnectionStub::OnRemoteRequest(uint32_t code, MessageParcel &data, MessageParcel &reply,
                                           MessageOption &option)
{
    return IPCObjectStub::OnRemoteRequest(code, data, reply, option);
}

std::mutex DataObsMgrClient::mutex_;
std::shared_ptr<DataObsMgrClient> DataObsMgrClient::instance_ = std::make_shared<DataObsMgrClient>();
DataObsMgrClient::DataObsMgrClient() {}
DataObsMgrClient::~DataObsMgrClient() {}
std::shared_ptr<DataObsMgrClient> DataObsMgrClient::GetInstance() { return instance_; }
ErrCode DataObsMgrClient::RegisterObserver(const Uri &uri, sptr<IDataAbilityObserver> dataObserver)
{
    return 0;
}
ErrCode DataObsMgrClient::UnregisterObserver(const Uri &uri, sptr<IDataAbilityObserver> dataObserver)
{
    return 0;
}
ErrCode DataObsMgrClient::NotifyChange(const Uri &uri) { return 0; }
Status DataObsMgrClient::RegisterObserverExt(const Uri &uri, sptr<IDataAbilityObserver> dataObserver,
    bool isDescendants)
{
    return SUCCESS;
}
Status DataObsMgrClient::UnregisterObserverExt(const Uri &uri, sptr<IDataAbilityObserver> dataObserver)
{
    return SUCCESS;
}
Status DataObsMgrClient::UnregisterObserverExt(sptr<IDataAbilityObserver> dataObserver)
{
    return SUCCESS;
}
Status DataObsMgrClient::NotifyChangeExt(const ChangeInfo &changeInfo)
{
    return SUCCESS;
}
DataAbilityObserverStub::DataAbilityObserverStub() {}
DataAbilityObserverStub::~DataAbilityObserverStub() {}
int DataAbilityObserverStub::OnRemoteRequest(uint32_t code, MessageParcel &data, MessageParcel &reply,
                                             MessageOption &option)
{
    return IPCObjectStub::OnRemoteRequest(code, data, reply, option);
}
int DataAbilityObserverStub::OnChangeInner(MessageParcel &data, MessageParcel &reply) { return 0; }
}
