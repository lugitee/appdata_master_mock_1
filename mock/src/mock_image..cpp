/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "pixel_map.h"
#include "pixel_map_napi.h"
namespace OHOS::Media {
std::shared_ptr<PixelMap> PixelMapNapi::GetPixelMap(napi_env env, napi_value pixelmap) { return nullptr; }
napi_value PixelMapNapi::CreatePixelMap(napi_env env, std::shared_ptr<PixelMap> pixelmap) { return nullptr; }
bool PixelMap::Marshalling(Parcel &data) const
{
    data.WriteInt32(imageInfo_.size.width);
    data.WriteInt32(imageInfo_.size.height);
    data.WriteInt32(int32_t(imageInfo_.pixelFormat));
    return true;
}

PixelMap *PixelMap::Unmarshalling(Parcel &data)
{
    ImageInfo imageInfo;
    data.ReadInt32(imageInfo.size.width);
    data.ReadInt32(imageInfo.size.height);
    imageInfo.pixelFormat = PixelFormat(data.ReadInt32());
    auto pixelMap = new PixelMap();
    pixelMap->SetImageInfo(imageInfo);
    return pixelMap;
}

PixelMap::~PixelMap() {}
std::unique_ptr<PixelMap> PixelMap::Create(const uint32_t *colors, uint32_t colorLength,
    const InitializationOptions &opts)
{
    return PixelMap::Create(colors, colorLength, 0, opts.size.width, opts);
}
std::unique_ptr<PixelMap> PixelMap::Create(const uint32_t *colors, uint32_t colorLength, int32_t offset,
    int32_t stride, const InitializationOptions &opts)
{
    auto dstPixelMap = std::make_unique<PixelMap>();
    ImageInfo imageInfo;
    imageInfo.size = opts.size;
    imageInfo.pixelFormat = opts.pixelFormat;
    dstPixelMap->SetImageInfo(imageInfo);
    return dstPixelMap;
}
std::unique_ptr<PixelMap> PixelMap::Create(const InitializationOptions &opts)
{
    return std::unique_ptr<PixelMap>();
}
std::unique_ptr<PixelMap> PixelMap::Create(PixelMap &source, const InitializationOptions &opts)
{
    return std::unique_ptr<PixelMap>();
}
std::unique_ptr<PixelMap> PixelMap::Create(PixelMap &source, const Rect &srcRect, const InitializationOptions &opts)
{
    return std::unique_ptr<PixelMap>();
}
uint32_t PixelMap::SetImageInfo(ImageInfo &info)
{
    imageInfo_ = info;
    return 0;
}
uint32_t PixelMap::SetImageInfo(ImageInfo &info, bool isReused)
{
    return 0;
}
const uint8_t *PixelMap::GetPixel(int32_t x, int32_t y)
{
    return nullptr;
}
const uint8_t *PixelMap::GetPixel8(int32_t x, int32_t y)
{
    return nullptr;
}
const uint16_t *PixelMap::GetPixel16(int32_t x, int32_t y)
{
    return nullptr;
}
const uint32_t *PixelMap::GetPixel32(int32_t x, int32_t y)
{
    return nullptr;
}
bool PixelMap::GetARGB32Color(int32_t x, int32_t y, uint32_t &color)
{
    return false;
}
void PixelMap::SetPixelsAddr(void *addr, void *context, uint32_t size, AllocatorType type, CustomFreePixelMap func)
{
}
int32_t PixelMap::GetPixelBytes()
{
    return 0;
}
int32_t PixelMap::GetRowBytes()
{
    return 0;
}
int32_t PixelMap::GetByteCount()
{
    return 0;
}
int32_t PixelMap::GetWidth()
{
    return imageInfo_.size.width;
}
int32_t PixelMap::GetHeight()
{
    return imageInfo_.size.height;
}
int32_t PixelMap::GetBaseDensity()
{
    return 0;
}
void PixelMap::scale(float xAxis, float yAxis)
{
}
void PixelMap::translate(float xAxis, float yAxis)
{
}
void PixelMap::rotate(float degrees)
{
}
void PixelMap::flip(bool xAxis, bool yAxis)
{
}
uint32_t PixelMap::crop(const Rect &rect)
{
    return 0;
}
void PixelMap::GetImageInfo(ImageInfo &imageInfo)
{
    imageInfo = imageInfo_;
}
PixelFormat PixelMap::GetPixelFormat()
{
    return PixelFormat::NV12;
}
ColorSpace PixelMap::GetColorSpace()
{
    return ColorSpace::SMPTE_C;
}
AlphaType PixelMap::GetAlphaType()
{
    return AlphaType::IMAGE_ALPHA_TYPE_UNPREMUL;
}
uint32_t PixelMap::SetAlpha(const float percent)
{
    return 0;
}
const uint8_t *PixelMap::GetPixels()
{
    return nullptr;
}
uint8_t PixelMap::GetARGB32ColorA(uint32_t color)
{
    return 0;
}
uint8_t PixelMap::GetARGB32ColorR(uint32_t color)
{
    return 0;
}
uint8_t PixelMap::GetARGB32ColorG(uint32_t color)
{
    return 0;
}
uint8_t PixelMap::GetARGB32ColorB(uint32_t color)
{
    return 0;
}
bool PixelMap::IsSameImage(const PixelMap &other)
{
    return false;
}
uint32_t PixelMap::ReadPixels(const uint64_t &bufferSize, const uint32_t &offset, const uint32_t &stride,
    const Rect &region, uint8_t *dst)
{
    return 0;
}
uint32_t PixelMap::ReadPixels(const uint64_t &bufferSize, uint8_t *dst)
{
    return 0;
}
uint32_t PixelMap::ReadPixel(const Position &pos, uint32_t &dst)
{
    return 0;
}
uint32_t PixelMap::ResetConfig(const Size &size, const PixelFormat &format)
{
    return 0;
}
bool PixelMap::SetAlphaType(const AlphaType &alphaType)
{
    return false;
}
uint32_t PixelMap::WritePixel(const Position &pos, const uint32_t &color)
{
    return 0;
}
uint32_t PixelMap::WritePixels(const uint8_t *source, const uint64_t &bufferSize, const uint32_t &offset,
    const uint32_t &stride, const Rect &region)
{
    return 0;
}
uint32_t PixelMap::WritePixels(const uint8_t *source, const uint64_t &bufferSize)
{
    return 0;
}
bool PixelMap::WritePixels(const uint32_t &color)
{
    return false;
}
void PixelMap::FreePixelMap()
{
}
AllocatorType PixelMap::GetAllocatorType()
{
    return AllocatorType::DEFAULT;
}
void *PixelMap::GetFd() const
{
    return nullptr;
}
}