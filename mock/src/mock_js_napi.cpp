/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "napi/native_api.h"
#include "napi/native_common.h"
#include "napi/native_node_api.h"
#include <libuv/include/uv.h>
extern "C" {
napi_status napi_get_named_property(napi_env env, napi_value object, const char *utf8name, napi_value *result)
{
    return napi_ok;
}
napi_status napi_call_function(napi_env env, napi_value recv, napi_value func, size_t argc, const napi_value *argv,
    napi_value *result)
{
    return napi_ok;
}
napi_status napi_get_cb_info(napi_env env, napi_callback_info cbinfo, size_t *argc, napi_value *argv,
    napi_value *this_arg, void **data)
{
    return napi_ok;
}
napi_status napi_get_global(napi_env env, napi_value *result) { return napi_ok; }
napi_status napi_get_value_string_utf8(napi_env env, napi_value value, char *buf, size_t bufsize, size_t *result)
{
    return napi_ok;
}
napi_status napi_get_array_length(napi_env env, napi_value value, uint32_t *result) { return napi_ok; }
napi_status napi_is_typedarray(napi_env env, napi_value value, bool *result) { return napi_ok; }
napi_status napi_get_typedarray_info(napi_env env, napi_value typedarray, napi_typedarray_type *type, size_t *length,
    void **data, napi_value *arraybuffer, size_t *byte_offset) { return napi_ok; }
napi_status napi_get_element(napi_env env, napi_value object, uint32_t index, napi_value *result) { return napi_ok; }
napi_status napi_typeof(napi_env env, napi_value value, napi_valuetype *result) { return napi_ok; }
napi_status napi_get_value_bool(napi_env env, napi_value value, bool *result) { return napi_ok; }
napi_status napi_set_element(napi_env env, napi_value object, uint32_t index, napi_value value) { return napi_ok; }
napi_status napi_get_last_error_info(napi_env env, const napi_extended_error_info **result) { return napi_ok; }
napi_status napi_is_exception_pending(napi_env env, bool *result) { return napi_ok; }
napi_status napi_throw_error(napi_env env, const char *code, const char *msg) { return napi_ok; }
napi_status napi_get_value_double(napi_env env, napi_value value, double *result) { return napi_ok; }
napi_status napi_create_array_with_length(napi_env env, size_t length, napi_value *result) { return napi_ok; }
napi_status napi_create_string_utf8(napi_env env, const char *str, size_t length,
    napi_value *result) { return napi_ok; }
napi_status napi_create_arraybuffer(napi_env env, size_t byte_length, void **data,
    napi_value *result) { return napi_ok; }
napi_status napi_create_typedarray(napi_env env, napi_typedarray_type type, size_t length, napi_value arraybuffer,
    size_t byte_offset, napi_value *result) { return napi_ok; }
napi_status napi_create_int32(napi_env env, int32_t value, napi_value *result) { return napi_ok; }
napi_status napi_create_int64(napi_env env, int64_t value, napi_value *result) { return napi_ok; }
napi_status napi_create_double(napi_env env, double value, napi_value *result) { return napi_ok; }
napi_status napi_get_boolean(napi_env env, bool value, napi_value *result) { return napi_ok; }
napi_status napi_define_class(napi_env env, const char *utf8name, size_t length, napi_callback constructor,
    void *callback_data, size_t property_count, const napi_property_descriptor *properties,
    napi_value *result) { return napi_ok; }
napi_status napi_unwrap(napi_env env, napi_value obj, void **result) { return napi_ok; }
napi_status napi_delete_reference(napi_env env, napi_ref ref) { return napi_ok; }
napi_status napi_create_reference(napi_env env, napi_value value, uint32_t initial_refcount,
    napi_ref *result) { return napi_ok; }
napi_status napi_wrap(napi_env env, napi_value js_object, void *native_object, napi_finalize finalize_cb,
    void *finalize_hint, napi_ref *result) { return napi_ok; }
napi_status napi_get_undefined(napi_env env, napi_value* result) { return napi_ok; }
napi_status napi_create_async_work(napi_env env, napi_value async_resource, napi_value async_resource_name,
    napi_async_execute_callback execute, napi_async_complete_callback complete, void *data,
    napi_async_work *result) { return napi_ok; }
napi_status napi_set_named_property(napi_env env, napi_value object, const char *utf8name,
    napi_value value) { return napi_ok; }
napi_status napi_get_new_target(napi_env env, napi_callback_info cbinfo, napi_value *result) { return napi_ok; }
napi_status napi_get_reference_value(napi_env env, napi_ref ref, napi_value *result) { return napi_ok; }
napi_status napi_new_instance(napi_env env, napi_value constructor, size_t argc, const napi_value *argv,
    napi_value *result) { return napi_ok; }
napi_status napi_get_value_int32(napi_env env, napi_value value, int32_t *result) { return napi_ok; }
napi_status napi_reference_ref(napi_env env, napi_ref ref, uint32_t* result) { return napi_ok; }
napi_status napi_reference_unref(napi_env env, napi_ref ref, uint32_t* result) { return napi_ok; }
napi_status napi_get_property_names(napi_env env, napi_value object, napi_value *result) { return napi_ok; }
napi_status napi_get_property(napi_env env, napi_value object, napi_value key, napi_value *result) { return napi_ok; }
napi_status napi_delete_async_work(napi_env env, napi_async_work work) { return napi_ok; }
napi_status napi_create_error(napi_env env, napi_value code, napi_value msg, napi_value *result) { return napi_ok; }
napi_status napi_resolve_deferred(napi_env env, napi_deferred deferred, napi_value resolution) { return napi_ok; }
napi_status napi_reject_deferred(napi_env env, napi_deferred deferred, napi_value resolution) { return napi_ok; }
napi_status napi_queue_async_work(napi_env env, napi_async_work work) { return napi_ok; }
napi_status napi_create_promise(napi_env env, napi_deferred *deferred, napi_value *promise) { return napi_ok; }
napi_status napi_create_object(napi_env env, napi_value* result) { return napi_ok; }
napi_status napi_define_properties(napi_env env, napi_value object, size_t property_count,
    const napi_property_descriptor *properties) { return napi_ok; }
void napi_module_register(napi_module* mod) { }

napi_status napi_async_init(napi_env env, napi_value async_resource, napi_value async_resource_name,
    napi_async_context *result)
{
    return napi_escape_called_twice;
}
napi_status napi_async_destroy(napi_env env, napi_async_context async_context)
{
    return napi_escape_called_twice;
}
napi_status napi_make_callback(napi_env env, napi_async_context async_context, napi_value recv, napi_value func,
    size_t argc, const napi_value *argv, napi_value *result)
{
    return napi_escape_called_twice;
}
napi_status napi_create_buffer(napi_env env, size_t length, void **data, napi_value *result)
{
    return napi_escape_called_twice;
}
napi_status napi_create_external_buffer(napi_env env, size_t length, void *data, napi_finalize finalize_cb,
    void *finalize_hint, napi_value *result)
{
    return napi_escape_called_twice;
}
napi_status napi_create_buffer_copy(napi_env env, size_t length, const void *data, void **result_data,
    napi_value *result)
{
    return napi_escape_called_twice;
}
napi_status napi_is_buffer(napi_env env, napi_value value, bool *result)
{
    return napi_escape_called_twice;
}
napi_status napi_get_buffer_info(napi_env env, napi_value value, void **data, size_t *length)
{
    return napi_escape_called_twice;
}
napi_status napi_cancel_async_work(napi_env env, napi_async_work work)
{
    return napi_escape_called_twice;
}
napi_status napi_get_node_version(napi_env env, const napi_node_version **version)
{
    return napi_escape_called_twice;
}
napi_status napi_get_uv_event_loop(napi_env env, struct uv_loop_s **loop)
{
    return napi_escape_called_twice;
}
napi_status napi_get_null(napi_env env, napi_value *result)
{
    return napi_escape_called_twice;
}
napi_status napi_create_array(napi_env env, napi_value *result)
{
    return napi_escape_called_twice;
}
napi_status napi_create_uint32(napi_env env, uint32_t value, napi_value *result)
{
    return napi_escape_called_twice;
}
napi_status napi_create_string_latin1(napi_env env, const char *str, size_t length, napi_value *result)
{
    return napi_escape_called_twice;
}
napi_status napi_create_string_utf16(napi_env env, const char16_t *str, size_t length, napi_value *result)
{
    return napi_escape_called_twice;
}
napi_status napi_create_symbol(napi_env env, napi_value description, napi_value *result)
{
    return napi_escape_called_twice;
}
napi_status napi_create_function(napi_env env, const char *utf8name, size_t length, napi_callback cb, void *data,
    napi_value *result)
{
    return napi_escape_called_twice;
}
napi_status napi_create_type_error(napi_env env, napi_value code, napi_value msg, napi_value *result)
{
    return napi_escape_called_twice;
}
napi_status napi_create_range_error(napi_env env, napi_value code, napi_value msg, napi_value *result)
{
    return napi_escape_called_twice;
}
napi_status napi_get_value_uint32(napi_env env, napi_value value, uint32_t *result)
{
    return napi_escape_called_twice;
}
napi_status napi_get_value_int64(napi_env env, napi_value value, int64_t *result)
{
    return napi_escape_called_twice;
}
napi_status napi_get_value_string_latin1(napi_env env, napi_value value, char *buf, size_t bufsize, size_t *result)
{
    return napi_escape_called_twice;
}
napi_status napi_get_value_string_utf16(napi_env env, napi_value value, char16_t *buf, size_t bufsize, size_t *result)
{
    return napi_escape_called_twice;
}
napi_status napi_coerce_to_bool(napi_env env, napi_value value, napi_value *result)
{
    return napi_escape_called_twice;
}
napi_status napi_coerce_to_number(napi_env env, napi_value value, napi_value *result)
{
    return napi_escape_called_twice;
}
napi_status napi_coerce_to_object(napi_env env, napi_value value, napi_value *result)
{
    return napi_escape_called_twice;
}
napi_status napi_coerce_to_string(napi_env env, napi_value value, napi_value *result)
{
    return napi_escape_called_twice;
}
napi_status napi_get_prototype(napi_env env, napi_value object, napi_value *result)
{
    return napi_escape_called_twice;
}
napi_status napi_set_property(napi_env env, napi_value object, napi_value key, napi_value value)
{
    return napi_escape_called_twice;
}
napi_status napi_has_property(napi_env env, napi_value object, napi_value key, bool *result)
{
    return napi_escape_called_twice;
}
napi_status napi_delete_property(napi_env env, napi_value object, napi_value key, bool *result)
{
    return napi_escape_called_twice;
}
napi_status napi_has_own_property(napi_env env, napi_value object, napi_value key, bool *result)
{
    return napi_escape_called_twice;
}
napi_status napi_has_named_property(napi_env env, napi_value object, const char *utf8name, bool *result)
{
    return napi_escape_called_twice;
}
napi_status napi_has_element(napi_env env, napi_value object, uint32_t index, bool *result)
{
    return napi_escape_called_twice;
}
napi_status napi_delete_element(napi_env env, napi_value object, uint32_t index, bool *result)
{
    return napi_escape_called_twice;
}
napi_status napi_is_array(napi_env env, napi_value value, bool *result)
{
    return napi_escape_called_twice;
}
napi_status napi_strict_equals(napi_env env, napi_value lhs, napi_value rhs, bool *result)
{
    return napi_escape_called_twice;
}
napi_status napi_instanceof(napi_env env, napi_value object, napi_value constructor, bool *result)
{
    return napi_escape_called_twice;
}
napi_status napi_remove_wrap(napi_env env, napi_value js_object, void **result)
{
    return napi_escape_called_twice;
}
napi_status napi_create_external(napi_env env, void *data, napi_finalize finalize_cb, void *finalize_hint,
    napi_value *result)
{
    return napi_escape_called_twice;
}
napi_status napi_get_value_external(napi_env env, napi_value value, void **result)
{
    return napi_escape_called_twice;
}
napi_status napi_open_handle_scope(napi_env env, napi_handle_scope *result)
{
    return napi_escape_called_twice;
}
napi_status napi_close_handle_scope(napi_env env, napi_handle_scope scope)
{
    return napi_escape_called_twice;
}
napi_status napi_open_escapable_handle_scope(napi_env env, napi_escapable_handle_scope *result)
{
    return napi_escape_called_twice;
}
napi_status napi_close_escapable_handle_scope(napi_env env, napi_escapable_handle_scope scope)
{
    return napi_escape_called_twice;
}
napi_status napi_escape_handle(napi_env env, napi_escapable_handle_scope scope, napi_value escapee, napi_value *result)
{
    return napi_escape_called_twice;
}
napi_status napi_throw(napi_env env, napi_value error)
{
    return napi_escape_called_twice;
}
napi_status napi_throw_type_error(napi_env env, const char *code, const char *msg)
{
    return napi_escape_called_twice;
}
napi_status napi_throw_range_error(napi_env env, const char *code, const char *msg)
{
    return napi_escape_called_twice;
}
napi_status napi_is_error(napi_env env, napi_value value, bool *result)
{
    return napi_escape_called_twice;
}
napi_status napi_get_and_clear_last_exception(napi_env env, napi_value *result)
{
    return napi_escape_called_twice;
}
napi_status napi_is_arraybuffer(napi_env env, napi_value value, bool *result)
{
    return napi_escape_called_twice;
}
napi_status napi_create_external_arraybuffer(napi_env env, void *external_data, size_t byte_length,
    napi_finalize finalize_cb, void *finalize_hint, napi_value *result)
{
    return napi_escape_called_twice;
}
napi_status napi_get_arraybuffer_info(napi_env env, napi_value arraybuffer, void **data, size_t *byte_length)
{
    return napi_escape_called_twice;
}
napi_status napi_create_dataview(napi_env env, size_t length, napi_value arraybuffer, size_t byte_offset,
    napi_value *result)
{
    return napi_escape_called_twice;
}
napi_status napi_is_dataview(napi_env env, napi_value value, bool *result)
{
    return napi_escape_called_twice;
}
napi_status napi_get_dataview_info(napi_env env, napi_value dataview, size_t *bytelength, void **data,
    napi_value *arraybuffer, size_t *byte_offset)
{
    return napi_escape_called_twice;
}
napi_status napi_get_version(napi_env env, uint32_t *result)
{
    return napi_escape_called_twice;
}
napi_status napi_is_promise(napi_env env, napi_value value, bool *is_promise)
{
    return napi_escape_called_twice;
}
napi_status napi_run_script(napi_env env, napi_value script, napi_value *result)
{
    return napi_escape_called_twice;
}
napi_status napi_adjust_external_memory(napi_env env, int64_t change_in_bytes, int64_t *adjusted_value)
{
    return napi_escape_called_twice;
}
UV_EXTERN int uv_queue_work(uv_loop_t* loop,
    uv_work_t* req,
    uv_work_cb work_cb,
    uv_after_work_cb after_work_cb) { return 0; };

NAPI_EXTERN napi_status napi_object_freeze(napi_env env,
    napi_value object)
{
    return napi_ok;
}
int uv_cancel(uv_req_t* req)
{
    return napi_ok;
}
}