/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "ohos_account_kits.h"
#include "os_account_manager.h"
class OhosAccountKitsImpl : public OHOS::AccountSA::OhosAccountKits {
public:
    std::pair<bool, OHOS::AccountSA::OhosAccountInfo> QueryOhosAccountInfo() override;
    bool UpdateOhosAccountInfo(const std::string &accountName, const std::string &uid,
                               const std::string &eventStr) override;
    OHOS::ErrCode QueryDeviceAccountId(int32_t &accountId) override;
    int32_t GetDeviceAccountIdByUID(int32_t &uid) override;
    std::pair<bool, OHOS::AccountSA::OhosAccountInfo> QueryOhosAccountInfoByUserId(std::int32_t userId) override;
};
namespace OHOS::AccountSA {
OhosAccountKits &OhosAccountKits::GetInstance()
{
    static OhosAccountKitsImpl instance;
    return instance;
}
ErrCode OsAccountManager::GetOsAccountLocalIdFromUid(const int uid, int &id)
{
    int32_t localUid = uid;
    id = OhosAccountKits::GetInstance().GetDeviceAccountIdByUID(localUid);
    return ERR_OK;
}

ErrCode OsAccountManager::QueryActiveOsAccountIds(std::vector<int32_t> &ids)
{
    ids = {0, 100, 101};
    return ERR_OK;
}

ErrCode OsAccountManager::IsOsAccountActived(const int id, bool &isOsAccountActived)
{
    isOsAccountActived = (id == 0 || id == 100);
    return ERR_OK;
}
}
std::pair<bool, OHOS::AccountSA::OhosAccountInfo> OhosAccountKitsImpl::QueryOhosAccountInfo()
{
    return std::pair<bool, OHOS::AccountSA::OhosAccountInfo>();
}
bool OhosAccountKitsImpl::UpdateOhosAccountInfo(const std::string &accountName, const std::string &uid,
                                                const std::string &eventStr)
{
    return false;
}
OHOS::ErrCode OhosAccountKitsImpl::QueryDeviceAccountId(int32_t &accountId)
{
    return 0;
}
int32_t OhosAccountKitsImpl::GetDeviceAccountIdByUID(int32_t &uid)
{
    return 0;
}

std::pair<bool, OHOS::AccountSA::OhosAccountInfo> OhosAccountKitsImpl::QueryOhosAccountInfoByUserId(std::int32_t userId)
{
    return std::pair<bool, OHOS::AccountSA::OhosAccountInfo>();
}
